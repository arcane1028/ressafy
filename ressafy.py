# -*- coding: utf-8 -*-
# 공유코드
import json
import re
import requests
from time import sleep
import urllib.request
import urllib.parse

from bs4 import BeautifulSoup
from flask import Flask, request, make_response
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter

SLACK_TOKEN = "xoxb-682997044292-689645874453-Nq51tebHJpHumrEsGDixz8Ff"
SLACK_SIGNING_SECRET = "b3c2235ed5f4723560eb6ff6d16878c3"
app = Flask(__name__)

# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

# 레시피를 순서를 알기 위한 전역변수
recipe_detail_num = {}
recipe_detail_list = {}
# 메시지를 두 번 나오는 것을 방지하는 전역변수
prev_msg_ids = {}


# 키워드로 레시피 사이트를 크롤링하여 레시피 정보들을 검색한 후,
# 메시지 블록을 만들어주는 함수입니다.
def make_sale_message_blocks(keywords):
    # print("make_sale_message_blocks")
    # 주어진 키워드로 레시피 사이트에서 크롤링합니다.
    search_keywords = ""
    for search_word in keywords:
        if search_keywords == "":
            search_keywords = search_word
        else:
            search_keywords = search_keywords + '+' + search_word

    # 검색 url을 만들어줍니다.
    query_text = urllib.parse.quote_plus(search_keywords)
    search_url = "http://www.10000recipe.com/recipe/list.html?q=" + query_text
    # ('Searching : ' + search_url)
    source_code = urllib.request.urlopen(search_url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    # 페이지에서 각 레시피 정보를 추출합니다.
    items = []
    # TODO
    for item_div in soup.find_all("a", class_="thumbnail", limit=10):
        title = item_div.find("div", class_="caption").find("h4").get_text().strip()
        link = item_div["href"]
        image = item_div.find("img", recursive=False)["src"]
        items.append({
            "title": title,
            "link": link,
            "image": image,
        })

    # 메시지를 꾸밉니다
    section_list = []

    # Divider Block
    divide = DividerBlock()

    # 검색 결과를 표시해주는 첫 번째 section
    search_keywords.replace('+', '/')
    head_section = SectionBlock(
        text=":cooking: *\"" + search_keywords + "\" " + " 에 대해 검색한 결과입니다.* :cooking: ",
    )

    section_list.append(head_section)
    section_list.append(divide)

    # 각 검색 결과에 대한 section을 만듭니다.
    for i, item in enumerate(items[:6]):
        # 요리에 대한 설명 + 사진에 대한 section을 만듭니다.
        item_image = ImageElement(
            image_url=item["image"],
            alt_text=item["title"]
        )

        item_button = ButtonElement(
            text="레시피보기",
            action_id=item["title"] + "_view", value=item["link"]
        )

        section_element = SectionBlock(
            text="*[" + item["title"] + "]* \n",
            accessory=item_image
        )

        button_section = SectionBlock(
            block_id="item_block" + str(i),
            text="해당 버튼을 눌러 레시피를 확인해보세요. :point_right: ",
            accessory=item_button
        )

        section_list.append(section_element)
        section_list.append(button_section)
        section_list.append(divide)

    # 각 섹션을 list로 묶어 전달합니다
    return section_list


# 챗봇이 멘션을 받으면 레시피 사이트를 검색합니다
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    # print("app_mentioned")
    client_msg_id = event_data["event"]["client_msg_id"]
    if client_msg_id in prev_msg_ids:
        print("prev msg")
        resp = make_response("no-retry", 201)
        resp.headers["X-Slack-No-Retry"] = 1
        return resp
    print("msg")
    prev_msg_ids[client_msg_id] = 1
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    ingredients = text.split(" ")[1:]
    url = "http://www.10000recipe.com/recipe/list.html?q="
    url3 = ""
    for ingredient in ingredients[:-1]:
        url3 += ingredient + "+"
    url3 += ingredients[-1]
    # print(url3)
    url2 = url + urllib.parse.quote_plus(url3)
    sourcecode = urllib.request.urlopen(url2).read()
    req = urllib.request.Request(url2)
    soup = BeautifulSoup(sourcecode, "html.parser")

    # 입력한 텍스트에서 검색 키워드와 가격대를 뽑아냅니다.
    if soup.find("div", class_="result_none"):
        # 유저에게 사용법을 알려줍니다
        # print(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text="결과가 없습니다. 사용하려면 `@chatbot <키워드> (+ <키워드>)`과 같이 멘션하세요"
        )
        return
    blocks = make_sale_message_blocks(ingredients)
    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(blocks)
    )


# '레시피 보기' 버튼을 누르면 해당 레시피를 가져옵니다. (간략하게)
def make_recipe_message_blocks(url):
    # print("make_recipe_message_blocks")

    req = urllib.request.Request(url)
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")
    image = soup.find("img", id="main_thumbs")["src"]
    title = soup.find("div", class_="view2_summary").h3.get_text()
    # 제목을 출력합니다.
    title_context = ContextBlock(
        elements=[
            {
                "type": "mrkdwn",
                "text": ":knife_fork_plate: * [ " + title + " ]* :knife_fork_plate: \n"
            }
        ]
    )
    # 레시피 이미지를 보여줍니다.
    first_item_image = ImageBlock(
        image_url=image,
        alt_text=title
    )

    # 간단한 내용을 보여줍니다.
    summary = soup.find("div", class_="view2_summary_in").get_text()

    if soup.find("span", class_="view2_summary_info1") is not None:
        inbun = soup.find("span", class_="view2_summary_info1").get_text()
        time = soup.find("span", class_="view2_summary_info2").get_text()
        diffculty = soup.find("span", class_="view2_summary_info3").get_text()
        contents_context = ContextBlock(
            elements=[
                {
                    "type": "mrkdwn",
                    "text": summary + "\n\n" + ":busts_in_silhouette:" + inbun + "\t" + ":timer_clock:" + time + "\t" + ":male-cook: :female-cook:" + diffculty
                }
            ]
        )
    else:
        contents_context = ContextBlock(
            elements=[
                {
                    "type": "mrkdwn",
                    "text": summary
                }
            ]
        )

    # 레시피를 자세히 보기 위한 버튼을 만듭니다.
    button_actions_summary = ActionsBlock(
        block_id="summary_block",
        elements=[
            ButtonElement(
                text="만들러가기",
                action_id="go_to_make", value=url
            )
        ]
    )
    ingre_fields = []
    i = 0
    if soup.find("div", class_="ready_ingre3") is not None:
        for ingres in soup.find("div", class_="ready_ingre3").find_all("ul"):
            for ingre in ingres.find_all("li"):
                if i == 10:
                    ingre_fields[9] = ingre_fields[9] + "<" + url + "|(재료가 더 있습니다...)>"
                    break
                i += 1
                # TODO
                # print(ingre.get_text(".").replace(" ", "").replace("\n", "").replace(".", "   "))
                # tmp = ingre.get_text("$").replace("\n", "").replace(" ", "").split("$")
                # print(tmp)
                # if len(tmp) == 2:
                #     ingre_fields.append("%s         %s" % (tmp[0], tmp[1]))
                # else:
                #     ingre_fields.append(tmp[0])
                ingre_fields.append(ingre.get_text(".").replace(" ", "").replace("\n", "").replace(".", "   "))
            if i == 10:
                break
    else:
        print("not")

    ingre_section = SectionBlock(fields=ingre_fields)
    return [title_context, DividerBlock(), first_item_image, contents_context, ingre_section,
            DividerBlock(), button_actions_summary]


# 자세한 레시피에 대해 알려줍니다 (순서)
def make_detailed_recipe_message_blocks(click_event, url_detail):
    # print(url_detail)
    req = urllib.request.Request(url_detail)
    sourcecode = urllib.request.urlopen(url_detail).read()
    soup = BeautifulSoup(sourcecode, "html.parser")
    section_list = []
    step_list = soup.find_all("div", {'id': re.compile(r"stepdescr\d+")})
    step_len = len(step_list)
    for i, text in enumerate(step_list, 1):
        item_image = None
        if soup.find("div", id="stepimg" + str(i)) is not None:
            item_image = ImageElement(
                image_url=soup.find("div", id="stepimg" + str(i)).img["src"],
                alt_text="stepimg" + str(i)
            )

        section_element = SectionBlock(
            text=str(i) + ". *" + text.get_text("$$$").split("$$$")[0] + "* \n",
            accessory=item_image
        )
        if not i == step_len:
            # 레시피를 자세히 보기 위한 버튼을 만듭니다.
            button_actions_detail = ActionsBlock(
                block_id="detail_block",
                elements=[
                    ButtonElement(
                        text="다음단계보기 "+str(i+1)+"/"+str(step_len),
                        action_id=url_detail, value="test"
                    )
                ]
            )
            section_list.append(section_element)
            section_list.append(button_actions_detail)
            section_list.append(DividerBlock())
        else:
            section_list.append(section_element)
            section_list.append(DividerBlock())
        # print(i + 1, text.get_text("$$$").split("$$$")[0])
    recipe_detail_num[url_detail] = 3
    recipe_detail_list[url_detail] = section_list
    # print("success")
    return section_list[0:3]


# 사용자가 버튼을 클릭한 결과는 /click 으로 받습니다
# 이 기능을 사용하려면 앱 설정 페이지의 "Interactive Components"에서
# /click 이 포함된 링크를 입력해야 합니다.
@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    #  print("on_button_click()")
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    click_id = click_event.block_id
    action_id = click_event.action_id

    # print(click_id)
    # 자세한 레시피 버튼 동작일 경우
    if click_id == 'summary_block':
        recipe_page_link = click_event.value
        message_blocks = make_detailed_recipe_message_blocks(click_event, recipe_page_link)

    # 간략 레시피 버튼 동작일 경우
    elif 'item_block' in click_id:
        # TODO
        # button을 누른 해당 레시피를 가져옵니다
        recipe_page_link = 'http://www.10000recipe.com' + click_event.value
        message_blocks = make_recipe_message_blocks(recipe_page_link)
    elif action_id in recipe_detail_num:
        cur_num = recipe_detail_num[action_id]
        message_blocks = recipe_detail_list[action_id][cur_num:cur_num+3]
        recipe_detail_num[action_id] += 3
    else:
        message_blocks =[]
        print("on_button_click ERROR")

    print(message_blocks)
    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(message_blocks)
    )

    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
